+ Incomplete (un)escaping of characters:

  - Character escapes in URIs with uppercase hexadecimal digits aren't handled.
  - Query parameters aren't unescaped properly.
  - Only a handful och characters are escaped in directory listings.

+ Resumed transfers are extremely slow.
+ Redirects do not handle HTTP/0.9 clients.
+ Valid ".." in filenames are rejected?
+ Assumes ISO-Latin-1 encoding?
+ Silence errors to stderr from pushd
  ::

      list:pushd:25: no such file or directory:
      /var/www/bar Error: Could not change to directory.
+ Add unit tests for encoding/decoding functions
