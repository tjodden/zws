==============
ZWS Change Log
==============

Unreleased
----------

Incompatibilities:

+ The script has been renamed to lowercase ``zws``.
+ The default root directory is now lowercase ``www``.
+ The default log filename is now lowercase ``zws.log``.

General:

+ External files are no longer needed for error pages, so the script is now
  completely self-contained which simplifies installation. Just copy ``zws``
  wherever you want, and run it.
+ Improved performance by replacing some usage of ``tr``, ``cut`` etc with
  various forms of parameter expansions, and by returning function results via
  a ``REPLY`` variable instead of echoing to stdout.
+ Removed dependencies on ``grep``, ``cut``, ``tr`` and ``date``.

Additions:

+ Added a ``Dockerfile`` and published the container image to Docker Hub.
  Now you can easily run ZWS as a container in the cloud (or locally)!

Exterminations:

+ Dates in headers could be sent with localized month and day names depending
  on the user's locale settings (``LC_*``, ``LANG``, etc) which is incorrect.
+ Date and time in the ``Date`` header were missing the trailing "GMT".
+ HTTP/1.0 redirects were erronously sent to HTTP/0.9 clients.


1.2 (2006-03-22)
----------------

General:

+ Removed dependency on basename and dirname commands.
+ Changed to usage of /usr/bin/env to find zsh instead of hardcoding the path.
+ Removed an undocumented option that was used internally.
+ Improved code organization and comments.

Additions:

+ Added explanations of all options to the help message.
+ Implemented automatic redirection of requests for directories with the 
  trailing slash missing to paths with the slash appended. So, if "/example" is
  requested and such a directory exists, the client will be redirected to
  "/example/". This is the de-facto standard behaviour.

Exterminations:

+ Unknown options were silently ignored.
+ The help message printed "parse_options" instead of "ZWS" in the usage line.
+ Debug output from the directory listing function was saved to the wrong file
  if the log file was specified with a relative path.  
+ The documentation contained some typos and factual errors.


1.1 (2006-03-08)
----------------

Incompatibilities:

+ The option to specify the path to the log file has been changed from ``-d``
  to ``-l`` (``-d`` now enables debug mode).

Additions:

+ Wrote documentation. 

+ Implemented optional generation of directory listings. Features icons, 
  friendly path navigation and recursive searching from the current 
  directory.

+ Added the ``-d`` option to enable debug mode. Apart from writing debug 
  messages to the log file specified with ``-l``, this also makes the 
  server invoke itself as an external command for each connection to 
  facilitate easier testing while developing.

Exterminations:

+ When decoding the requested path only the first occurance of a particular
  character escape was replaced. For example, "a%20b%20c" would be decoded 
  to "a b%20c" instead of correctly to "a b c".

+ Error pages weren't sent because of a typo in the path name (the directory
  with the error pages is named "Errors", but the code thought it was named
  "errors").

+ The mime type wasn't detected correctly for MPEG files because 'file' prints
  garbage. Atleast version 4.13 on Gentoo is affected. This is worked around by 
  hardcoding the mime type of files named *.mpeg to video/mpeg.

+ The emulation mode and required options weren't set at the beginning of the
  script, which could lead to incorrect behaviour for people with strange (ie., 
  different than mine) configurations.
  

1.0 (2004-05-29)
----------------

Initial public release.
