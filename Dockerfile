FROM tjodden/zsh
RUN mkdir /var/www
COPY zws /usr/sbin
EXPOSE 80
ENTRYPOINT ["/usr/sbin/zws", "-p", "80", "-r", "/var/www"]
