====================
ZWS (ZSH Web Server)
====================

ZWS is a simple web server written in zsh. It implements HTTP/0.9 and a small
subset of HTTP 1.0 and 1.1 but works well with most clients anyway. It can
optionally generate directory listings (which also provides an integrated
search functionality).


Features
========

+ Requires only a recent version of ZSH and some common UNIX commands.
+ Supports HTTP/0.9 and a small subset of HTTP/1.0 and HTTP/1.1:

  - Sends ``Date``, ``Last-Modified`` and ``Content-Length`` header fields.
  - Supports resumed transfers, albeit with poor performance (sends 
    ``Accept-Ranges`` and ``Content-Range`` fields, reads and parses the 
    ``Range`` field).
  - Replaces printable characters escaped with %XX in URLs.

+ Does a simple check to dissallow reading files from outside the web root.
+ Optionally generates directory listings (with integreated search).


Requirements
============

+ ZSH 4.2.0 or later
+ The UNIX commands dd, sed and file.


Installation
============

Just copy the ``zws`` script wherever you want.


Options
=======

``-d``
    Enable debug mode. Various debug messages will be written to the log file,
    and the server will also invoke itself as an external command for each
    incoming connection. This is slower but useful when developing since the
    server doesn't have to be restarted to test changes.

``-i``
    Enable automatic directory listing (with integrated search). Listings for
    directories without an index file (ie. ``index.html``) will automatically
    be generated. WARNING: This also enables the search feature, which might
    potentially be a security risk (the search terms are passed verbatim to the
    globbing functionality of ZSH).

``-l FILE`` [default: zws.log]
    Specify the path to the log file (not used unless ``-d`` is also used).

``-p PORT`` [default: 4280]
    Specify the port to listen on for incoming connections.

``-r DIRECTORY`` [default: www]
    Specify the web root directory. Files will be served from this and all
    subdirectories. 


Known Issues
============

+ Incomplete (un)escaping of characters:
  
  - Character escapes in URIs with uppercase hexadecimal digits aren't handled.
  - Query parameters aren't unescaped properly.
  - Only a handful och characters are escaped in directory listings.

+ Resumed transfers are extremely slow.
+ Redirects do not handle HTTP/0.9 clients.


Thanks
======

I would like to thank Carl Drougge for reporting bugs and suggesting how to fix
some of them.


Legal
=====

This program is copyright © 2004-2019, Adam Chodorowski. All rights reserved.
It is distributed under the terms of version 2 of the GNU General Public
License. Please see the ``LICENSE`` file for the complete license text or
read it online at http://www.gnu.org/licenses/gpl-2.0.html.
